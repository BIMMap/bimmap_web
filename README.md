# BIMMap
## Environments ##
Hoje contamos com 3 ambientes para desenvolver \n
  - Produção
  - Beta
  - Dev

Para melhor atender ao cliente final e a equipe de desenvolvimento os ambientes são organizados para as seguintes funções

#### Environment Dev

- Nome do db: bimma243_dev
- Senha: PERGUNTE AO ADM DE BANCO DE DADOS
- Url de acesso(server): dev.bimmap.com.br
#### Environment Beta
- Nome do db: bimma243_dev
- Senha: PERGUNTE AO ADM DE BANCO DE DADOS
- Url de acesso(server): dev.bimmap.com.br
-
### Version
- **1**.2.3: Versão reescrita na integra do projeto,
    - Entre uma versão 1.0 e 2.0 necessariamente deve ter sido reconstruído partes           significativas do software.
- 1.**2**.3: Corresponde a implementação de novos módulos.
- 1.2.**3**: São versões de correção de bugs ou pequenos ajustes.

 -  ##### Production -> 1.0
 -  ##### Developer -> 1.1.0
 - ##### Beta -> 1.1.0

### Tech

BIMMap usa algumas tecnologias open source:

* [AngularJS] - HTML enhanced for web apps!
* [Twitter Bootstrap] - great UI boilerplate for modern web apps
* [CakePhp ] - Framework php
* [jQuery] - duh

### Installation
Você precisa ter instalado em seu computador:
    - GIT
> building


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does it's job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [dill]: <https://github.com/joemccann/dillinger>
   [CakePhp]: <http://cakephp.org/>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [john gruber]: <http://daringfireball.net>
   [@thomasfuchs]: <http://twitter.com/thomasfuchs>
   [df1]: <http://daringfireball.net/projects/markdown/>
   [marked]: <https://github.com/chjj/marked>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [keymaster.js]: <https://github.com/madrobby/keymaster>
   [jQuery]: <http://jquery.com>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]:  <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>


>>>>>>> Stashed changes